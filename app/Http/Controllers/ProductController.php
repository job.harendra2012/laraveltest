<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ProductRequest;
use Illuminate\Support\Facades\Storage;
class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $products = Storage::disk('local')->exists('products.json') ? json_decode(Storage::disk('local')->get('products.json')) : []; 
      $data['products'] = array_reverse($products);
      return view('index',$data); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['url'] = route('product.store');
        $data['title'] = 'Add New Product';
        $data['buttonTitle'] ='Submit';
        $data['method'] ='POST';
        return view('product-form',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {
        try {
            $products = Storage::disk('local')->exists('products.json') ? json_decode(Storage::disk('local')->get('products.json')) : [];
            $inputs = $request->only(['product_name', 'quantity_in_stock', 'price_per_item']);
            $inputs['datetime_submitted'] = date('Y-m-d H:i:s');
            array_push($products,$inputs);
            Storage::disk('local')->put('products.json', json_encode($products));
            $products = Storage::disk('local')->exists('products.json') ? json_decode(Storage::disk('local')->get('products.json')) :[]; 
            $data['products'] = array_reverse($products);
            $view = view('product-list',$data);
            return response()->json(['status' => true, 'view' => $view->render()],200);
        } catch(Exception $e) {
            return response()->json(['status' => false, 'message' => $e->getMessage()],200);
        }
    }
}
