<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product_name'=>'required|min:2|max:50',
            'quantity_in_stock'=>'required|integer',
            'price_per_item'=>'required|numeric'
        ];
    }
    public function messages()
    {
        return [
            'product_name.required' => 'Please enter a product name',
            'product_name.min' => 'Product name must consist of at least 2 characters',
            'product_name.max' => 'Product name must be less than 50 characters'
              ];
    }




}
