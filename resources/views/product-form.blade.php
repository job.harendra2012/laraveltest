<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal">&times;</button>
  <h4 class="modal-title">{{$title}}</h4>
</div>
<div class="modal-body">
  <form action="{{$url}}" method="POST" id="frmProduct">
   <input type="hidden" name="_method" value="{{$method}}" />
    @csrf
    <div class="form-group">
      <label for="product_name">Product name:</label>
      <input type="text" class="form-control" name="product_name" placeholder="Product name" required>
    </div>
    <div class="form-group">
      <label for="quantity_in_stock">Quantity in stock:</label>
      <input type="number" class="form-control" name="quantity_in_stock" required>
    </div>
    <div class="form-group">
      <label for="price_per_item">Price per item:</label>
      <input type="text" class="form-control" name="price_per_item" required>
    </div>
    <button type="submit" class="btn btn-default" id="btnSubmit">{{$buttonTitle}}</button>
  </form>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>
