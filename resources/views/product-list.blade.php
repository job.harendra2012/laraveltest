  <table class="table table-bordered" >
    <thead>
      <tr>
        <th>Product name</th>
        <th>Quantity in stock</th>
        <th>Price per item</th>
        <th>Datetime submitted</th>
        <th>Total value number</th>
      </tr>
    </thead>
    <tbody>
      @if(count($products)>0)
      	@foreach($products as $row)

          <tr>
            <td>{{$row->product_name}}</td>
            <td>{{$row->quantity_in_stock}}</td>
            <td>${{$row->price_per_item}}</td>
            <td>{{$row->datetime_submitted}}</td>
            <td>${{$row->price_per_item*$row->quantity_in_stock}}</td>
          </tr>
        @endforeach
       @else
       	<tr><td colspan="5">No records found.</td></tr>
       @endif
    </tbody>
  </table>
