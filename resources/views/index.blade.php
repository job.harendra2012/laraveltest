<!DOCTYPE html>
<html lang="en">
<head>
<title>Product List</title>
<meta charset="utf-8">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<script>
	window.Laravel = {!! json_encode([
		'csrfToken' => csrf_token(),
	]) !!};
</script>
</head>
<body>
<div class="container">
  <h2>Product List</h2>
  <p>
    <button type="button pull-right" class="btn btn-default" id="btnAdd">Add New</button>
  </p>
  <div class="row" id="product-list">@include('product-list')</div>
</div>
<div id="productModel" class="modal fade" role="dialog">
  <div class="modal-dialog"> 
    <div class="modal-content" id="productModelContent">
    </div>
  </div>
</div>
<script>
$(document).ready(function() {
	$("#btnAdd").click(function(e){
		e.preventDefault();
		$.ajax({
			type: "GET",
			beforeSend: function () {
				$(this).html('Processing...');
				$(this).prop('disabled', true);
			},                         
			url:'{{route("product.create")}}',
			success: function (response) {
				$('#productModelContent').html(response);
				$('#productModel').modal('show');
			},
			error: function (response){

				},
			complete:function(){
				$(this).html('Add New');
				$(this).prop('disabled', false);
			}
		});
		return false;
	});
	$("body").on('submit','#frmProduct',function(e){
		e.preventDefault();
		$.ajaxSetup({
		  headers: {
			  'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
		  }
		});	
		var buttonTitle = 	$("#btnSubmit").text();
		$.ajax({
			type: "POST",
			dataType: "json",
			data: $(this).serialize(),
			beforeSend: function () {
				$( "div" ).remove( ".invalid-feedback" );
				$("#btnSubmit").html('Processing...');
				$("#btnSubmit").prop('disabled', true);
			},                         
			url: $(this).attr('action'),
			success: function (response) {
				if(response.status==true)
				{
					$('#product-list').html(response.view);
				}				
				if(response.status==false)
				{
					 alert(response.message);
				}
				$('#productModel').modal('hide');
				$('#productModelContent').html('');
			},
			error: function (response){
					if (response.readyState == 4 && response.status == 422) {
						var myObj = JSON.parse(response.responseText);
						$.each( myObj.errors, function( key, value ) {							 $("input[name='"+key+"']").parent().parent().addClass('has-error');
							 $("input[name='"+key+"']").after('<div class="invalid-feedback">'+value+'</div>');
						});
					}
				},
			complete:function(){
				$("#btnSubmit").html('Add New');
				$("#btnSubmit").prop('disabled', false);
			}
		});//endajax
		return false;
	});//end submit
} );  
</script>
</body>
</html>
